import {
  NgModule
} from '@angular/core';
import {
  BrowserModule
} from '@angular/platform-browser';

import {
  AppRoutingModule
} from './app-routing.module';
import {
  AppComponent
} from './app.component';
import {
  BrowserAnimationsModule
} from '@angular/platform-browser/animations';
import {
  FlexLayoutModule
} from '@angular/flex-layout';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  HttpModule
} from '@angular/http';
import {
  HttpClientModule
} from '@angular/common/http';

import {
  HomeComponent
} from './views/home/home.component';
import {
  AuthComponent
} from './views/auth/auth.component';
import {
  NotfoundComponent
} from './views/notfound/notfound.component';
import {
  AppMaterialModule
} from './app-material.module';
import {
  LandingscreenComponent
} from './views/home/landingscreen/landingscreen.component';
import {
  AdminscreenComponent
} from './views/home/adminscreen/adminscreen.component';

import {
  SignInService
} from './services/auth/signin.service';
import {
  RegisterationService
} from './services/auth/registeration.service';
import {
  MAT_DATE_LOCALE
} from '@angular/material';
import {
  BookServiceDialogComponent
} from './components/dialogs/book-service-dialog/book-service-dialog.component';
import {
  BookServiceService
} from './services/book-service/book-service.service';
import {
  UserManagementComponent
} from './views/home/adminscreen/user-management/user-management.component';
import {
  RequestManagementComponent
} from './views/home/adminscreen/request-management/request-management.component';
import {
  UserDetailService
} from './services/auth/user-detail.service';
import {
  ManageManagerComponent
} from './views/home/adminscreen/user-management/manage-manager/manage-manager.component';
import {
  ManageProviderComponent
} from './views/home/adminscreen/user-management/manage-provider/manage-provider.component';
import {
  ServiceConfigService
} from './services/service-config/service-config.service';
import {
  UserManagementService
} from './services/user-management/user-management.service';
import {
  UserprofileComponent
} from './views/home/userprofile/userprofile.component';
import {
  UserDetailDialogComponent
} from './components/dialogs/user-detail-dialog/user-detail-dialog.component';
import {
  UserDeleteDialogComponent
} from './components/dialogs/user-delete-dialog/user-delete-dialog.component';
import {
  UserUpdateDialogComponent
} from './components/dialogs/user-update-dialog/user-update-dialog.component';
import {
  ContactDialogComponent
} from './components/dialogs/contact-dialog/contact-dialog.component';
import {
  PasswordManagementService
} from './services/auth/password-management.service';
import { MobileVerificationDialogComponent } from './components/dialogs/mobile-verification-dialog/mobile-verification-dialog.component';
import { UserQuestionDialogComponent } from './components/dialogs/user-question-dialog/user-question-dialog.component';
import { AssignProviderDialogComponent } from './components/dialogs/assign-provider-dialog/assign-provider-dialog.component';
import { ScheduleAppointmentDialogComponent } from './components/dialogs/schedule-appointment-dialog/schedule-appointment-dialog.component';
import { RequestDetailDialogComponent } from './components/dialogs/request-detail-dialog/request-detail-dialog.component';
import { RequestActionsDialogComponent } from './components/dialogs/request-actions-dialog/request-actions-dialog.component';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { GenericStatusDialogComponent } from './components/dialogs/generic-status-dialog/generic-status-dialog.component';
import { ProviderIdentityDialogComponent } from './components/dialogs/provider-identity-dialog/provider-identity-dialog.component';
import { BookingCommentsDialogComponent } from './components/dialogs/booking-comments-dialog/booking-comments-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthComponent,
    NotfoundComponent,
    LandingscreenComponent,
    AdminscreenComponent,
    BookServiceDialogComponent,
    UserManagementComponent,
    RequestManagementComponent,
    ManageManagerComponent,
    ManageProviderComponent,
    UserprofileComponent,
    UserDetailDialogComponent,
    UserDeleteDialogComponent,
    UserUpdateDialogComponent,
    ContactDialogComponent,
    MobileVerificationDialogComponent,
    UserQuestionDialogComponent,
    AssignProviderDialogComponent,
    ScheduleAppointmentDialogComponent,
    RequestDetailDialogComponent,
    RequestActionsDialogComponent,
    GenericStatusDialogComponent,
    ProviderIdentityDialogComponent,
    BookingCommentsDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
  ],
  providers: [
    SignInService,
    RegisterationService,
    BookServiceService,
    UserDetailService,
    ServiceConfigService,
    UserManagementService,
    PasswordManagementService,
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'en-GB'
    },
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    BookServiceDialogComponent,
    UserDetailDialogComponent,
    UserDeleteDialogComponent,
    UserUpdateDialogComponent,
    ContactDialogComponent,
    MobileVerificationDialogComponent,
    UserQuestionDialogComponent,
    AssignProviderDialogComponent,
    RequestActionsDialogComponent,
    RequestDetailDialogComponent,
    ScheduleAppointmentDialogComponent,
    GenericStatusDialogComponent,
    ProviderIdentityDialogComponent,
    BookingCommentsDialogComponent
  ]
})
export class AppModule {}
