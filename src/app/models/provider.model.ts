export class ProviderElement {
    'id': number;
    'first_name': string;
    'last_name': string;
    'email': string;
    'mobile': string;
    'speciality': string;
}
