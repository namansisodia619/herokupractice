export class ServiceElement {
    'id': number;
    'servicename': string;
    'phyReq': boolean;
    'gynReq': boolean;
    'obsReq': boolean;
    'midreq': boolean;
}
