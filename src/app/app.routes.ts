import {
  Routes
} from '@angular/router';
import {
  HomeComponent
} from './views/home/home.component';
import {
  AuthComponent
} from './views/auth/auth.component';
import {
  AdminscreenComponent
} from './views/home/adminscreen/adminscreen.component';
import {
  LandingscreenComponent
} from './views/home/landingscreen/landingscreen.component';
import {
  UserManagementComponent
} from './views/home/adminscreen/user-management/user-management.component';
import {
  RequestManagementComponent
} from './views/home/adminscreen/request-management/request-management.component';
import {
  ManageManagerComponent
} from './views/home/adminscreen/user-management/manage-manager/manage-manager.component';
import {
  ManageProviderComponent
} from './views/home/adminscreen/user-management/manage-provider/manage-provider.component';
import { UserprofileComponent } from './views/home/userprofile/userprofile.component';


export const routes: Routes = [{
    path: '',
    redirectTo: '/auth',
    pathMatch: 'full',
  },
  {
    path: 'auth',
    component: AuthComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    children: [{
        path: '',
        component: LandingscreenComponent
      },
      {
        path: 'dashboard',
        component: LandingscreenComponent
      },
      {
        path: 'userprofile',
        component: UserprofileComponent
      },
      {
        path: 'admin',
        children: [{
            path: 'user-management',
            component: UserManagementComponent
          },
          {
            path: 'request-management',
            component: RequestManagementComponent
          },
          {
            path: 'manage-manager',
            component: ManageManagerComponent
          },
          {
            path: 'manage-provider',
            component: ManageProviderComponent
          }
        ]
      }
    ]
  },
];
