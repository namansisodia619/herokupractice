import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { AuthComponent } from './views/auth/auth.component';
import { NotfoundComponent } from './views/notfound/notfound.component';
import { LandingscreenComponent } from './views/home/landingscreen/landingscreen.component';
import { AdminscreenComponent } from './views/home/adminscreen/adminscreen.component';
import { BookServiceDialogComponent } from './components/dialogs/book-service-dialog/book-service-dialog/book-service-dialog.component';
import { UserManagementComponent } from './views/home/adminscreen/user-management/user-management.component';
import { RequestManagementComponent } from './views/home/adminscreen/request-management/request-management.component';
import { ManageManagerComponent } from './views/home/adminscreen/user-management/manage-manager/manage-manager.component';
import { ManageProviderComponent } from './views/home/adminscreen/user-management/manage-provider/manage-provider.component';
import { UserprofileComponent } from './views/home/userprofile/userprofile.component';
import { UserDetailDialogComponent } from './components/dialogs/user-detail-dialog/user-detail-dialog.component';
import { UserDeleteDialogComponent } from './components/dialogs/user-delete-dialog/user-delete-dialog.component';
import { UserUpdateDialogComponent } from './components/dialogs/user-update-dialog/user-update-dialog.component';
import { ContactDialogComponent } from './components/dialogs/contact-dialog/contact-dialog.component';
import { MobileVerificationDialogComponent } from './components/dialogs/mobile-verification-dialog/mobile-verification-dialog.component';
import { UserQuestionDialogComponent } from './components/dialogs/user-question-dialog/user-question-dialog.component';
import { AssignProviderDialogComponent } from './components/dialogs/assign-provider-dialog/assign-provider-dialog.component';
import { ScheduleAppointmentDialogComponent } from './components/dialogs/schedule-appointment-dialog/schedule-appointment-dialog.component';
import { RequestDetailDialogComponent } from './components/dialogs/request-detail-dialog/request-detail-dialog.component';
import { RequestActionsDialogComponent } from './components/dialogs/request-actions-dialog/request-actions-dialog.component';
import { GenericStatusDialogComponent } from './components/dialogs/generic-status-dialog/generic-status-dialog.component';
import { ProviderIdentityDialogComponent } from './components/dialogs/provider-identity-dialog/provider-identity-dialog.component';
import { BookingCommentsDialogComponent } from './components/dialogs/booking-comments-dialog/booking-comments-dialog.component';


// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthComponent,
    NotfoundComponent,
    LandingscreenComponent,
    AdminscreenComponent,
    BookServiceDialogComponent,
    UserManagementComponent,
    RequestManagementComponent,
    ManageManagerComponent,
    ManageProviderComponent,
    UserprofileComponent,
    UserDetailDialogComponent,
    UserDeleteDialogComponent,
    UserUpdateDialogComponent,
    ContactDialogComponent,
    MobileVerificationDialogComponent,
    UserQuestionDialogComponent,
    AssignProviderDialogComponent,
    ScheduleAppointmentDialogComponent,
    RequestDetailDialogComponent,
    RequestActionsDialogComponent,
    GenericStatusDialogComponent,
    ProviderIdentityDialogComponent,
    BookingCommentsDialogComponent,
  ],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})

export class AppModule { }

