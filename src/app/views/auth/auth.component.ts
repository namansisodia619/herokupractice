import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  ActivatedRoute
} from '@angular/router';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  SignInService
} from '../../services/auth/signin.service';
import {
  RegisterationService
} from '../../services/auth/registeration.service';
import {
  UserDetailService
} from '../../services/auth/user-detail.service';
import {
  PasswordManagementService
} from '../../services/auth/password-management.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  providers: [SignInService]
})
export class AuthComponent implements OnInit {

  hide = true;
  validUser = true;
  validUserEmail = true;
  loadingProgress = false;
  registrationSuccess = false;
  registrationFailure = false;
  maxDate = new Date(Date.now());
  startDate = this.maxDate;
  isForgotPassword = false;
  isChangePassword = false;
  isPasswordVerificationLinkSent = false;
  validUserEmailForReset = true;
  firstParamEmail = null;
  secondParamKey = null;
  isResetSuccess = false;
  isResetFailure = false;

  userDetail = {
    userName: '',
    password: ''
  };

  forgotPasswordDetail = {
    password: '',
    userName: '',
    verificationString: ''
  };

  resetPasswordDetail = {
    password: '',
    userName: '',
    verificationString: ''
  };

  userRegistrationDetail = {
    firstName: '',
    lastName: '',
    userName: '',
    password: '',
    phoneNumber: '',
    gender: '',
    dob: '',
    address1: '',
    address2: '',
    country: '',
    state: '',
    city: '',
    pincode: '',
    roleName: [''],
    userType: 'ROLE_USER'
  };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private signInService: SignInService,
    private localStorage: LocalStorage,
    private registrationService: RegisterationService,
    private userDetailService: UserDetailService,
    private passwordManagementService: PasswordManagementService
  ) {}

  ngOnInit() {
    this.firstParamEmail = this.activatedRoute.snapshot.queryParamMap.get('email');
    this.secondParamKey = this.activatedRoute.snapshot.queryParamMap.get('key');
    this.localStorage.clear().subscribe(() => {});
    this.maxDate.setDate(this.maxDate.getDate());
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
    this.startDate = this.maxDate;
    this.changePasswordScreen();
  }

  gotoHome = function () {
    this.router.navigateByUrl('/home');
  };

  gotoServiceRequests = function () {
    this.router.navigateByUrl('home/admin/request-management');
  };

  gotoForgotPassword = function () {
    this.isForgotPassword = true;
  };

  goBackToSignin = function () {
    this.isForgotPassword = false;
    this.isChangePassword = false;
  };

  loginUser() {
    this.loadingProgress = true;
    this.localStorage.clear().subscribe(() => {});
    this.signInService.loginUser(this.userDetail).subscribe(
      data => {
        this.loadingProgress = false;
        const userData: any = data;
        this.localStorage.setItem('userData', userData).subscribe(() => {

          let userNameObj, tokenValue;
          userNameObj = {
            userName: userData.result.username
          };
          tokenValue = userData.result.token;
          this.userDetailService.getUserDetail(userNameObj, tokenValue).subscribe(
            (dataValue: any) => {
              this.localStorage.setItem('userDetails', dataValue).subscribe(() => {
                this.validUser = true;
                if (dataValue.userType === 'USER') {
                  this.gotoHome();
                } else {
                  this.gotoServiceRequests();
                }
              });
            },
            error => {
              console.error('Error Loggin In!');
            }
          );
        });
      },
      error => {
        this.loadingProgress = false;
        console.error('Error Loggin In!');
        this.validUser = false;
      }
    );
  }

  registerUser() {

    this.loadingProgress = true;

    this.registrationService.registerUser(this.userRegistrationDetail).subscribe(
      data => {
        this.loadingProgress = false;
        this.registrationSuccess = true;
      },
      error => {
        this.loadingProgress = false;
        this.registrationFailure = true;
        console.error('Error Loggin In!');
      }
    );
  }

  sendVerificationLink() {
    this.loadingProgress = true;

    this.passwordManagementService.sendResetPasswordLink(this.forgotPasswordDetail).subscribe(
      data => {
        this.loadingProgress = false;
        this.isPasswordVerificationLinkSent = true;
      },
      error => {
        this.loadingProgress = false;
        this.validUserEmailForReset = false;
        console.error('Error Loggin In!');
      }
    );
  }

  changePasswordScreen() {
    if (this.firstParamEmail) {
      this.isChangePassword = true;
      this.resetPasswordDetail.userName = this.firstParamEmail;
    }
  }

  resetPassword = function () {
    this.loadingProgress = true;

    this.resetPasswordDetail.verificationString = this.secondParamKey;

    this.passwordManagementService.changePassword(this.resetPasswordDetail).subscribe(
      data => {
        this.loadingProgress = false;
        this.isResetSuccess = true;
      },
      error => {
        this.loadingProgress = false;
        this.isResetFailure = true;
        console.error('Error Loggin In!');
      });
  };

}
