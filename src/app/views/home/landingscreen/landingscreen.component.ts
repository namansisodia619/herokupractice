import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormControl
} from '@angular/forms';
import {
  Observable
} from 'rxjs';
import {
  map,
  startWith
} from 'rxjs/operators';
import {
  MatDialog,
  MatDialogConfig
} from '@angular/material';
import {
  BookServiceDialogComponent
} from '../../../components/dialogs/book-service-dialog/book-service-dialog.component';
import {
  UserQuestionDialogComponent
} from '../../../components/dialogs/user-question-dialog/user-question-dialog.component';
import {
  Router
} from '@angular/router';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  ServiceConfigService
} from '../../../../app/services/service-config/service-config.service';
import { BookServiceService } from '../../../../app/services/book-service/book-service.service';

@Component({
  selector: 'app-landingscreen',
  templateUrl: './landingscreen.component.html',
  styleUrls: ['./landingscreen.component.scss']
})
export class LandingscreenComponent implements OnInit {
  myControl = new FormControl();
  options: string[] = ['Natural IUI', 'Medical IUI', 'IVF', 'Egg Donation', 'Egg Freezing', 'Surrogacy'];
  filteredOptions: Observable < string[] > ;
  serviceList = [];
  bookedServicesList = [];

  quoteArray = [{
    'quote': 'Infertility is just a hurdle, not a roadblock',
    'quoteBy': 'Anonymous'
  }, {
    'quote': 'Today, social stigma in India is delaying the detection of male infertility by ~9 months, resulting in a missed birth cycle',
    'quoteBy': 'Anonymous'
  }, {
    'quote': 'In 90% cases, Couples can overcome infertility through lifestyle changes',
    'quoteBy': 'Anonymous'
  }, {
    'quote': 'In 40% case, Male partner is either the sole or contributing cause of infertility',
    'quoteBy': 'Anonymous'
  }, {
    'quote': 'To win the battle, conceive in mind first The belief will translate to the womb',
    'quoteBy': 'Anonymous'
  }, {
    'quote': 'Yes, it is possible to treat infertility at the privacy of home - choose the right healthcare services',
    'quoteBy': 'Anonymous'
  }, {
    'quote': 'IUI in a relaxed environment like home naturally improves chances of conception',
    'quoteBy': 'Anonymous'
  }, {
    'quote': 'Women with body weight disorders can overcome infertility challenges with right guidance',
    'quoteBy': 'Anonymous'
  }, {
    'quote': 'Quit Smoking - up to 13 percent of female infertility is caused by cigarette smoking',
    'quoteBy': 'Anonymous'
  }, {
    'quote': 'If you are over 35 years see a fertility specialist after six months of trying for a baby',
    'quoteBy': 'Anonymous'
  }, {
    'quote': 'Every child comes with the message that God is not yet discouraged of man',
    'quoteBy': 'Rabindranath Tagore'
  }, {
    'quote': 'Babies are such a nice way to start people',
    'quoteBy': 'Don Herold'
  }, {
    'quote': 'Like stars are to the sky, so are the children to our world They deserve to shine!',
    'quoteBy': 'Chinonye J Chidolue'
  }, {
    'quote': 'It is a smile of a baby that makes life worth living',
    'quoteBy': 'Debasish Mridha'
  }, {
    'quote': 'To profoundly understand a baby, you must first be a mother',
    'quoteBy': 'Debasish Mridha'
  }, {
    'quote': 'Babies are living dolls with dancing smiles that come from the stars to steal our hearts',
    'quoteBy': 'Debasish Mridha'
  }, {
    'quote': 'Babies are the buds of life ready to bloom like a fresh flower to refresh humanity',
    'quoteBy': 'Debasish Mridha'
  }, {
    'quote': 'A baby is a dancing joy of life',
    'quoteBy': 'Debasish Mridha'
  }, {
    'quote': 'A baby is as pure as an angel and as fresh as a blooming flower',
    'quoteBy': 'Debasish Mridha'
  }, {
    'quote': 'You know what the great thing about babies is? They are like little bundles of hope Like the future in a basket',
    'quoteBy': 'Lish McBride'
  }, {
    'quote': 'For me, this baby was the most precious thing I had ever had He was my treasure, my joy, my world, my everything now',
    'quoteBy': 'Preeti Shenoy'
  }];

  quoteArrayItem = {
    'quote': 'Babies are the buds of imagination that are ready to bloom with lights of love and affection.',
    'quoteBy': 'Debasish Mridha'
  };

  ngOnInit() {
    this.quoteArrayItem = this.quoteArray[Math.floor(Math.random() * this.quoteArray.length)];
    this.getServiceList();
    this.getServiceBookedList();
  }

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private serviceConfigService: ServiceConfigService,
    private bookServiceService: BookServiceService,
    private localStorage: LocalStorage
  ) {}

  openBookingDialog(valueString): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      serviceDetail: valueString
    };

    dialogConfig.disableClose = true;

    const dialogRef = this.dialog.open(BookServiceDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.getServiceBookedList();
    });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  getServiceList = function () {
    let tokenValue;
    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.serviceConfigService.getServiceConfigs(tokenValue).subscribe(
        data => {
          this.serviceList = data;
        },
        error => {
          console.error('Error Loggin In!');
        }
      );
    });

  };

  getServiceBookedList = function () {
    let tokenValue;
    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      const userNameDetail = {
        userName: userData.result.username
      };

      this.bookServiceService.getServiceBookingListByUserName(userNameDetail, tokenValue).subscribe(
        data => {
          this.bookedServicesList = data;
        },
        error => {
          console.error('Error Loggin In!');
        }
      );
    });

  };

  openQuestionsDialog = function (bookingId: string) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '500px';
    dialogConfig.data = {
      bookingId: bookingId
    };

    const dialogRef = this.dialog.open(UserQuestionDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  };

}
