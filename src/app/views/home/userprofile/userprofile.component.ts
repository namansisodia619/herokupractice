import {
  Component,
  OnInit
} from '@angular/core';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  UserDetailService
} from '../../../services/auth/user-detail.service';
import {
  Router
} from '@angular/router';
import {
  MatDialogConfig,
  MatDialog
} from '@angular/material';
import {
  UserUpdateDialogComponent
} from '../../../components/dialogs/user-update-dialog/user-update-dialog.component';
import {
  MobileVerificationDialogComponent
} from '../../../components/dialogs/mobile-verification-dialog/mobile-verification-dialog.component';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.scss']
})
export class UserprofileComponent implements OnInit {

  userProfileDetail = {
    active: false,
    address: {},
    firstName: '',
    id: null,
    lastName: '',
    roles: {},
    userDOB: '',
    userGender: '',
    userName: '',
    userPhoneNumber: '',
    userType: '',
    mobileVerify: false
  };

  mobileVerification = {
    otp: ''
  };

  userPartnerProfileDetail = {
    address: {},
    dob: '',
    email: '',
    firstName: '',
    gender: '',
    id: null,
    lastName: '',
    phoneNumber: ''
  };

  addSpouseDetail = {
    address1: '',
    address2: '',
    associatedUser: '',
    city: '',
    country: '',
    dob: '',
    email: '',
    firstName: '',
    gender: '',
    lastName: '',
    phoneNumber: '',
    pincode: '',
    state: ''
  };

  stateList = [{
      code: 'AP',
      name: 'Andhra Pradesh'
    },
    {
      code: 'AR',
      name: 'Arunachal Pradesh'
    },
    {
      code: 'AS',
      name: 'Assam'
    },
    {
      code: 'BR',
      name: 'Bihar'
    },
    {
      code: 'CG',
      name: 'Chhattisgarh'
    },
    {
      code: 'Chandigarh',
      name: 'Chandigarh'
    },
    {
      code: 'DN',
      name: 'Dadra and Nagar Haveli'
    },
    {
      code: 'DD',
      name: 'Daman and Diu'
    },
    {
      code: 'DL',
      name: 'Delhi'
    },
    {
      code: 'GA',
      name: 'Goa'
    },
    {
      code: 'GJ',
      name: 'Gujarat'
    },
    {
      code: 'HR',
      name: 'Haryana'
    },
    {
      code: 'HP',
      name: 'Himachal Pradesh'
    },
    {
      code: 'JK',
      name: 'Jammu and Kashmir'
    },
    {
      code: 'JH',
      name: 'Jharkhand'
    },
    {
      code: 'KA',
      name: 'Karnataka'
    },
    {
      code: 'KL',
      name: 'Kerala'
    },
    {
      code: 'MP',
      name: 'Madhya Pradesh'
    },
    {
      code: 'MH',
      name: 'Maharashtra'
    },
    {
      code: 'MN',
      name: 'Manipur'
    },
    {
      code: 'ML',
      name: 'Meghalaya'
    },
    {
      code: 'MZ',
      name: 'Mizoram'
    },
    {
      code: 'NL',
      name: 'Nagaland'
    },
    {
      code: 'OR',
      name: 'Orissa'
    },
    {
      code: 'PB',
      name: 'Punjab'
    },
    {
      code: 'PY',
      name: 'Pondicherry'
    },
    {
      code: 'RJ',
      name: 'Rajasthan'
    },
    {
      code: 'SK',
      name: 'Sikkim'
    },
    {
      code: 'TN',
      name: 'Tamil Nadu'
    },
    {
      code: 'TR',
      name: 'Tripura'
    },
    {
      code: 'UP',
      name: 'Uttar Pradesh'
    },
    {
      code: 'UK',
      name: 'Uttarakhand'
    },
    {
      code: 'WB',
      name: 'West Bengal'
    }
  ];

  countryList = [{
      name: 'Afghanistan',
      code: 'AF'
    },
    {
      name: 'Åland Islands',
      code: 'AX'
    },
    {
      name: 'Albania',
      code: 'AL'
    },
    {
      name: 'Algeria',
      code: 'DZ'
    },
    {
      name: 'American Samoa',
      code: 'AS'
    },
    {
      name: 'AndorrA',
      code: 'AD'
    },
    {
      name: 'Angola',
      code: 'AO'
    },
    {
      name: 'Anguilla',
      code: 'AI'
    },
    {
      name: 'Antarctica',
      code: 'AQ'
    },
    {
      name: 'Antigua and Barbuda',
      code: 'AG'
    },
    {
      name: 'Argentina',
      code: 'AR'
    },
    {
      name: 'Armenia',
      code: 'AM'
    },
    {
      name: 'Aruba',
      code: 'AW'
    },
    {
      name: 'Australia',
      code: 'AU'
    },
    {
      name: 'Austria',
      code: 'AT'
    },
    {
      name: 'Azerbaijan',
      code: 'AZ'
    },
    {
      name: 'Bahamas',
      code: 'BS'
    },
    {
      name: 'Bahrain',
      code: 'BH'
    },
    {
      name: 'Bangladesh',
      code: 'BD'
    },
    {
      name: 'Barbados',
      code: 'BB'
    },
    {
      name: 'Belarus',
      code: 'BY'
    },
    {
      name: 'Belgium',
      code: 'BE'
    },
    {
      name: 'Belize',
      code: 'BZ'
    },
    {
      name: 'Benin',
      code: 'BJ'
    },
    {
      name: 'Bermuda',
      code: 'BM'
    },
    {
      name: 'Bhutan',
      code: 'BT'
    },
    {
      name: 'Bolivia',
      code: 'BO'
    },
    {
      name: 'Bosnia and Herzegovina',
      code: 'BA'
    },
    {
      name: 'Botswana',
      code: 'BW'
    },
    {
      name: 'Bouvet Island',
      code: 'BV'
    },
    {
      name: 'Brazil',
      code: 'BR'
    },
    {
      name: 'British Indian Ocean Territory',
      code: 'IO'
    },
    {
      name: 'Brunei Darussalam',
      code: 'BN'
    },
    {
      name: 'Bulgaria',
      code: 'BG'
    },
    {
      name: 'Burkina Faso',
      code: 'BF'
    },
    {
      name: 'Burundi',
      code: 'BI'
    },
    {
      name: 'Cambodia',
      code: 'KH'
    },
    {
      name: 'Cameroon',
      code: 'CM'
    },
    {
      name: 'Canada',
      code: 'CA'
    },
    {
      name: 'Cape Verde',
      code: 'CV'
    },
    {
      name: 'Cayman Islands',
      code: 'KY'
    },
    {
      name: 'Central African Republic',
      code: 'CF'
    },
    {
      name: 'Chad',
      code: 'TD'
    },
    {
      name: 'Chile',
      code: 'CL'
    },
    {
      name: 'China',
      code: 'CN'
    },
    {
      name: 'Christmas Island',
      code: 'CX'
    },
    {
      name: 'Cocos (Keeling) Islands',
      code: 'CC'
    },
    {
      name: 'Colombia',
      code: 'CO'
    },
    {
      name: 'Comoros',
      code: 'KM'
    },
    {
      name: 'Congo',
      code: 'CG'
    },
    {
      name: 'Congo, The Democratic Republic of the',
      code: 'CD'
    },
    {
      name: 'Cook Islands',
      code: 'CK'
    },
    {
      name: 'Costa Rica',
      code: 'CR'
    },
    {
      name: 'Cote D\'Ivoire',
      code: 'CI'
    },
    {
      name: 'Croatia',
      code: 'HR'
    },
    {
      name: 'Cuba',
      code: 'CU'
    },
    {
      name: 'Cyprus',
      code: 'CY'
    },
    {
      name: 'Czech Republic',
      code: 'CZ'
    },
    {
      name: 'Denmark',
      code: 'DK'
    },
    {
      name: 'Djibouti',
      code: 'DJ'
    },
    {
      name: 'Dominica',
      code: 'DM'
    },
    {
      name: 'Dominican Republic',
      code: 'DO'
    },
    {
      name: 'Ecuador',
      code: 'EC'
    },
    {
      name: 'Egypt',
      code: 'EG'
    },
    {
      name: 'El Salvador',
      code: 'SV'
    },
    {
      name: 'Equatorial Guinea',
      code: 'GQ'
    },
    {
      name: 'Eritrea',
      code: 'ER'
    },
    {
      name: 'Estonia',
      code: 'EE'
    },
    {
      name: 'Ethiopia',
      code: 'ET'
    },
    {
      name: 'Falkland Islands (Malvinas)',
      code: 'FK'
    },
    {
      name: 'Faroe Islands',
      code: 'FO'
    },
    {
      name: 'Fiji',
      code: 'FJ'
    },
    {
      name: 'Finland',
      code: 'FI'
    },
    {
      name: 'France',
      code: 'FR'
    },
    {
      name: 'French Guiana',
      code: 'GF'
    },
    {
      name: 'French Polynesia',
      code: 'PF'
    },
    {
      name: 'French Southern Territories',
      code: 'TF'
    },
    {
      name: 'Gabon',
      code: 'GA'
    },
    {
      name: 'Gambia',
      code: 'GM'
    },
    {
      name: 'Georgia',
      code: 'GE'
    },
    {
      name: 'Germany',
      code: 'DE'
    },
    {
      name: 'Ghana',
      code: 'GH'
    },
    {
      name: 'Gibraltar',
      code: 'GI'
    },
    {
      name: 'Greece',
      code: 'GR'
    },
    {
      name: 'Greenland',
      code: 'GL'
    },
    {
      name: 'Grenada',
      code: 'GD'
    },
    {
      name: 'Guadeloupe',
      code: 'GP'
    },
    {
      name: 'Guam',
      code: 'GU'
    },
    {
      name: 'Guatemala',
      code: 'GT'
    },
    {
      name: 'Guernsey',
      code: 'GG'
    },
    {
      name: 'Guinea',
      code: 'GN'
    },
    {
      name: 'Guinea-Bissau',
      code: 'GW'
    },
    {
      name: 'Guyana',
      code: 'GY'
    },
    {
      name: 'Haiti',
      code: 'HT'
    },
    {
      name: 'Heard Island and Mcdonald Islands',
      code: 'HM'
    },
    {
      name: 'Holy See (Vatican City State)',
      code: 'VA'
    },
    {
      name: 'Honduras',
      code: 'HN'
    },
    {
      name: 'Hong Kong',
      code: 'HK'
    },
    {
      name: 'Hungary',
      code: 'HU'
    },
    {
      name: 'Iceland',
      code: 'IS'
    },
    {
      name: 'India',
      code: 'IN'
    },
    {
      name: 'Indonesia',
      code: 'ID'
    },
    {
      name: 'Iran, Islamic Republic Of',
      code: 'IR'
    },
    {
      name: 'Iraq',
      code: 'IQ'
    },
    {
      name: 'Ireland',
      code: 'IE'
    },
    {
      name: 'Isle of Man',
      code: 'IM'
    },
    {
      name: 'Israel',
      code: 'IL'
    },
    {
      name: 'Italy',
      code: 'IT'
    },
    {
      name: 'Jamaica',
      code: 'JM'
    },
    {
      name: 'Japan',
      code: 'JP'
    },
    {
      name: 'Jersey',
      code: 'JE'
    },
    {
      name: 'Jordan',
      code: 'JO'
    },
    {
      name: 'Kazakhstan',
      code: 'KZ'
    },
    {
      name: 'Kenya',
      code: 'KE'
    },
    {
      name: 'Kiribati',
      code: 'KI'
    },
    {
      name: 'Korea, Democratic People\'S Republic of',
      code: 'KP'
    },
    {
      name: 'Korea, Republic of',
      code: 'KR'
    },
    {
      name: 'Kuwait',
      code: 'KW'
    },
    {
      name: 'Kyrgyzstan',
      code: 'KG'
    },
    {
      name: 'Lao People\'S Democratic Republic',
      code: 'LA'
    },
    {
      name: 'Latvia',
      code: 'LV'
    },
    {
      name: 'Lebanon',
      code: 'LB'
    },
    {
      name: 'Lesotho',
      code: 'LS'
    },
    {
      name: 'Liberia',
      code: 'LR'
    },
    {
      name: 'Libyan Arab Jamahiriya',
      code: 'LY'
    },
    {
      name: 'Liechtenstein',
      code: 'LI'
    },
    {
      name: 'Lithuania',
      code: 'LT'
    },
    {
      name: 'Luxembourg',
      code: 'LU'
    },
    {
      name: 'Macao',
      code: 'MO'
    },
    {
      name: 'Macedonia, The Former Yugoslav Republic of',
      code: 'MK'
    },
    {
      name: 'Madagascar',
      code: 'MG'
    },
    {
      name: 'Malawi',
      code: 'MW'
    },
    {
      name: 'Malaysia',
      code: 'MY'
    },
    {
      name: 'Maldives',
      code: 'MV'
    },
    {
      name: 'Mali',
      code: 'ML'
    },
    {
      name: 'Malta',
      code: 'MT'
    },
    {
      name: 'Marshall Islands',
      code: 'MH'
    },
    {
      name: 'Martinique',
      code: 'MQ'
    },
    {
      name: 'Mauritania',
      code: 'MR'
    },
    {
      name: 'Mauritius',
      code: 'MU'
    },
    {
      name: 'Mayotte',
      code: 'YT'
    },
    {
      name: 'Mexico',
      code: 'MX'
    },
    {
      name: 'Micronesia, Federated States of',
      code: 'FM'
    },
    {
      name: 'Moldova, Republic of',
      code: 'MD'
    },
    {
      name: 'Monaco',
      code: 'MC'
    },
    {
      name: 'Mongolia',
      code: 'MN'
    },
    {
      name: 'Montserrat',
      code: 'MS'
    },
    {
      name: 'Morocco',
      code: 'MA'
    },
    {
      name: 'Mozambique',
      code: 'MZ'
    },
    {
      name: 'Myanmar',
      code: 'MM'
    },
    {
      name: 'Namibia',
      code: 'NA'
    },
    {
      name: 'Nauru',
      code: 'NR'
    },
    {
      name: 'Nepal',
      code: 'NP'
    },
    {
      name: 'Netherlands',
      code: 'NL'
    },
    {
      name: 'Netherlands Antilles',
      code: 'AN'
    },
    {
      name: 'New Caledonia',
      code: 'NC'
    },
    {
      name: 'New Zealand',
      code: 'NZ'
    },
    {
      name: 'Nicaragua',
      code: 'NI'
    },
    {
      name: 'Niger',
      code: 'NE'
    },
    {
      name: 'Nigeria',
      code: 'NG'
    },
    {
      name: 'Niue',
      code: 'NU'
    },
    {
      name: 'Norfolk Island',
      code: 'NF'
    },
    {
      name: 'Northern Mariana Islands',
      code: 'MP'
    },
    {
      name: 'Norway',
      code: 'NO'
    },
    {
      name: 'Oman',
      code: 'OM'
    },
    {
      name: 'Pakistan',
      code: 'PK'
    },
    {
      name: 'Palau',
      code: 'PW'
    },
    {
      name: 'Palestinian Territory, Occupied',
      code: 'PS'
    },
    {
      name: 'Panama',
      code: 'PA'
    },
    {
      name: 'Papua New Guinea',
      code: 'PG'
    },
    {
      name: 'Paraguay',
      code: 'PY'
    },
    {
      name: 'Peru',
      code: 'PE'
    },
    {
      name: 'Philippines',
      code: 'PH'
    },
    {
      name: 'Pitcairn',
      code: 'PN'
    },
    {
      name: 'Poland',
      code: 'PL'
    },
    {
      name: 'Portugal',
      code: 'PT'
    },
    {
      name: 'Puerto Rico',
      code: 'PR'
    },
    {
      name: 'Qatar',
      code: 'QA'
    },
    {
      name: 'Reunion',
      code: 'RE'
    },
    {
      name: 'Romania',
      code: 'RO'
    },
    {
      name: 'Russian Federation',
      code: 'RU'
    },
    {
      name: 'RWANDA',
      code: 'RW'
    },
    {
      name: 'Saint Helena',
      code: 'SH'
    },
    {
      name: 'Saint Kitts and Nevis',
      code: 'KN'
    },
    {
      name: 'Saint Lucia',
      code: 'LC'
    },
    {
      name: 'Saint Pierre and Miquelon',
      code: 'PM'
    },
    {
      name: 'Saint Vincent and the Grenadines',
      code: 'VC'
    },
    {
      name: 'Samoa',
      code: 'WS'
    },
    {
      name: 'San Marino',
      code: 'SM'
    },
    {
      name: 'Sao Tome and Principe',
      code: 'ST'
    },
    {
      name: 'Saudi Arabia',
      code: 'SA'
    },
    {
      name: 'Senegal',
      code: 'SN'
    },
    {
      name: 'Serbia and Montenegro',
      code: 'CS'
    },
    {
      name: 'Seychelles',
      code: 'SC'
    },
    {
      name: 'Sierra Leone',
      code: 'SL'
    },
    {
      name: 'Singapore',
      code: 'SG'
    },
    {
      name: 'Slovakia',
      code: 'SK'
    },
    {
      name: 'Slovenia',
      code: 'SI'
    },
    {
      name: 'Solomon Islands',
      code: 'SB'
    },
    {
      name: 'Somalia',
      code: 'SO'
    },
    {
      name: 'South Africa',
      code: 'ZA'
    },
    {
      name: 'South Georgia and the South Sandwich Islands',
      code: 'GS'
    },
    {
      name: 'Spain',
      code: 'ES'
    },
    {
      name: 'Sri Lanka',
      code: 'LK'
    },
    {
      name: 'Sudan',
      code: 'SD'
    },
    {
      name: 'Suriname',
      code: 'SR'
    },
    {
      name: 'Svalbard and Jan Mayen',
      code: 'SJ'
    },
    {
      name: 'Swaziland',
      code: 'SZ'
    },
    {
      name: 'Sweden',
      code: 'SE'
    },
    {
      name: 'Switzerland',
      code: 'CH'
    },
    {
      name: 'Syrian Arab Republic',
      code: 'SY'
    },
    {
      name: 'Taiwan, Province of China',
      code: 'TW'
    },
    {
      name: 'Tajikistan',
      code: 'TJ'
    },
    {
      name: 'Tanzania, United Republic of',
      code: 'TZ'
    },
    {
      name: 'Thailand',
      code: 'TH'
    },
    {
      name: 'Timor-Leste',
      code: 'TL'
    },
    {
      name: 'Togo',
      code: 'TG'
    },
    {
      name: 'Tokelau',
      code: 'TK'
    },
    {
      name: 'Tonga',
      code: 'TO'
    },
    {
      name: 'Trinidad and Tobago',
      code: 'TT'
    },
    {
      name: 'Tunisia',
      code: 'TN'
    },
    {
      name: 'Turkey',
      code: 'TR'
    },
    {
      name: 'Turkmenistan',
      code: 'TM'
    },
    {
      name: 'Turks and Caicos Islands',
      code: 'TC'
    },
    {
      name: 'Tuvalu',
      code: 'TV'
    },
    {
      name: 'Uganda',
      code: 'UG'
    },
    {
      name: 'Ukraine',
      code: 'UA'
    },
    {
      name: 'United Arab Emirates',
      code: 'AE'
    },
    {
      name: 'United Kingdom',
      code: 'GB'
    },
    {
      name: 'United States',
      code: 'US'
    },
    {
      name: 'United States Minor Outlying Islands',
      code: 'UM'
    },
    {
      name: 'Uruguay',
      code: 'UY'
    },
    {
      name: 'Uzbekistan',
      code: 'UZ'
    },
    {
      name: 'Vanuatu',
      code: 'VU'
    },
    {
      name: 'Venezuela',
      code: 'VE'
    },
    {
      name: 'Viet Nam',
      code: 'VN'
    },
    {
      name: 'Virgin Islands, British',
      code: 'VG'
    },
    {
      name: 'Virgin Islands, U.S.',
      code: 'VI'
    },
    {
      name: 'Wallis and Futuna',
      code: 'WF'
    },
    {
      name: 'Western Sahara',
      code: 'EH'
    },
    {
      name: 'Yemen',
      code: 'YE'
    },
    {
      name: 'Zambia',
      code: 'ZM'
    },
    {
      name: 'Zimbabwe',
      code: 'ZW'
    }
  ];

  isUserProfile = false;
  loadingProgress = false;
  isEditable = false;
  isSpouseEditable = false;
  isSpouseRegister = false;
  maxDate = new Date(Date.now());
  startDate = this.maxDate;
  isSpouseDataUnavailable = false;
  // isOTPSent = false;

  constructor(
    private localStorage: LocalStorage,
    private router: Router,
    private userDetailService: UserDetailService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.getUserProfileDetails();
    this.maxDate.setDate(this.maxDate.getDate());
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
    this.startDate = this.maxDate;
  }
  getUserProfileDetails = function () {
    let tokenValue;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      this.localStorage.getItem('userDetails').subscribe((userDetails: any) => {
        this.userProfileDetail = userDetails;
        this.userProfileDetail.userDOB = new Date(this.userProfileDetail.userDOB);
        if (userDetails.userType === 'USER') {
          this.isUserProfile = true;
          const userNameObj = {
            userName: userDetails.userName
          };
          this.userDetailService.getUserPartnerDetail(userNameObj, tokenValue).subscribe(
            dataValue => {
              this.userPartnerProfileDetail = dataValue;
              this.userPartnerProfileDetail.dob = new Date(this.userPartnerProfileDetail.dob);
              this.isSpouseDataUnavailable = false;
            },
            error => {
              if (error.error.message === 'Spouse details not found') {
                this.isSpouseDataUnavailable = true;
              }
              console.error('Error getting Details!');
            });
        }
      });
    });
  };

  updateUserDetail = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      const updatedUserDetail = this.userProfileDetail;

      this.userDetailService.updateUserDetail(updatedUserDetail, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.localStorage.setItem('userDetails', data).subscribe(() => {
            this.getUserProfileDetails();
            this.isEditable = false;
            this.openStatusDialog(true);
          });
        },
        error => {
          this.loadingProgress = false;
          this.openStatusDialog(false);
          console.error('Error Loggin In!');
        }
      );
    });
  };

  sendOTPForMobileVerfication = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      this.localStorage.getItem('userDetails').subscribe((userDetails: any) => {

        const userMobileNumber = '+91' + userDetails.userPhoneNumber;
        this.userDetailService.sendOTPForMobileVerfication(userMobileNumber, tokenValue).subscribe(
          (data: any) => {
            this.loadingProgress = false;
            this.launchMobileVerificationDialog('sendOTPSuccess');
            console.log('OTP Sent');
          },
          error => {
            this.loadingProgress = false;
            this.launchMobileVerificationDialog('sendOTPFailure');
          });
      });
    });
  };

  verifyMobileOTP = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      this.localStorage.getItem('userDetails').subscribe((userDetails: any) => {

        if (userDetails.userType === 'USER') {
          const userMobileNumber = '+91' + userDetails.userPhoneNumber;
          const userName = userDetails.userName;
          this.userDetailService.verifyOTPForMobile(userMobileNumber, userName, this.mobileVerification, tokenValue).subscribe(
            data => {
              this.loadingProgress = false;
              this.userProfileDetail.mobileVerify = true;
              this.localStorage.setItem('userDetails', this.userProfileDetail).subscribe(() => {});
            },
            error => {
              this.loadingProgress = false;
              console.error(error);
            });
        }
      });
    });
  };

  addUserSpouseDetail = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.addSpouseDetail.address1 = this.userProfileDetail.address && this.userProfileDetail.address.address1;
      this.addSpouseDetail.address2 = this.userProfileDetail.address && this.userProfileDetail.address.address2;
      this.addSpouseDetail.city = this.userProfileDetail.address && this.userProfileDetail.address.city;
      this.addSpouseDetail.country = this.userProfileDetail.address && this.userProfileDetail.address.country;
      this.addSpouseDetail.pincode = this.userProfileDetail.address && this.userProfileDetail.address.pincode;
      this.addSpouseDetail.state = this.userProfileDetail.address && this.userProfileDetail.address.state;
      this.addSpouseDetail.associatedUser = this.userProfileDetail.userName;

      this.userDetailService.addSpouseDetail(this.addSpouseDetail, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.getUserProfileDetails();
          this.isEditable = false;
          this.openStatusDialog(true);
        },
        error => {
          this.loadingProgress = false;
          this.openStatusDialog(false);
          console.error('Error Loggin In!');
        }
      );
    });
  };

  updateUserSpouseDetail = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.userPartnerProfileDetail.address1 = this.userProfileDetail.address && this.userProfileDetail.address.address1;
      this.userPartnerProfileDetail.address2 = this.userProfileDetail.address && this.userProfileDetail.address.address2;
      this.userPartnerProfileDetail.city = this.userProfileDetail.address && this.userProfileDetail.address.city;
      this.userPartnerProfileDetail.country = this.userProfileDetail.address && this.userProfileDetail.address.country;
      this.userPartnerProfileDetail.pincode = this.userProfileDetail.address && this.userProfileDetail.address.pincode;
      this.userPartnerProfileDetail.state = this.userProfileDetail.address && this.userProfileDetail.address.state;
      this.userPartnerProfileDetail.associatedUser = this.userProfileDetail.userName;

      this.userDetailService.updateSpouseDetail(this.userPartnerProfileDetail, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.getUserProfileDetails();
          this.isSpouseEditable = false;
          this.openStatusDialog(true);
        },
        error => {
          this.loadingProgress = false;
          this.openStatusDialog(false);
          console.error('Error Loggin In!');
        }
      );
    });
  };

  editUserProfile = function (userType) {
    switch (userType) {
      case 'USER':
        this.isEditable = true;
        break;
      case 'SPOUSE':
        this.isSpouseEditable = true;
        break;
    }
  };

  addNewSpouseDetails = function () {
    this.isSpouseRegister = true;
    this.isSpouseDataUnavailable = true;
  };

  cancelNewSpouseDetails = function () {
    this.isSpouseRegister = false;
  };

  reloadUserProfile = function () {
    this.getUserProfileDetails();
    this.isEditable = false;
    this.isSpouseEditable = false;
  };

  openStatusDialog = function (dataValue) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      updateSuccess: dataValue,
      width: '500px',
    };

    this.dialog.open(UserUpdateDialogComponent, dialogConfig);
  };

  launchMobileVerificationDialog(statusValue): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      dialogStatus: statusValue,
      width: '500px',
    };

    const dialogRef = this.dialog.open(MobileVerificationDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {

      let tokenValue;
      this.loadingProgress = true;

      this.localStorage.getItem('userData').subscribe((userData: any) => {
        tokenValue = userData.result.token;
        const userNameObj = {
          userName: this.userProfileDetail.userName
        };
        this.userDetailService.getUserDetail(userNameObj, tokenValue).subscribe(
          (dataValue: any) => {
            this.localStorage.setItem('userDetails', dataValue).subscribe(() => {
              this.getUserProfileDetails();
            });
          },
          error => {
            console.error('Error Loggin In!');
          }
        );
      });
    });
  }

}
