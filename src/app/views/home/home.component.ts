import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router
} from '@angular/router';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  UserDetailService
} from '../../services/auth/user-detail.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { ContactDialogComponent } from '../../components/dialogs/contact-dialog/contact-dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  userDetails = '';
  isValidLogin = false;
  isUserTypeUser = true;

  constructor(
    private router: Router,
    private localStorage: LocalStorage,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.getUserDetails();
  }

  getUserDetails() {
    this.localStorage.getItem('userDetails').subscribe((userDetails: any) => {
      this.userDetails = userDetails;
      if (!userDetails) {
        this.router.navigateByUrl('/auth');
      } else {
        this.isValidLogin = true;
        if (userDetails.userType !== 'USER') {
          this.isUserTypeUser = false;
        }
      }
    });
  }

  gotoAuth = function () {
    this.router.navigateByUrl('/auth');
  };

  gotoHome = function () {
    this.router.navigateByUrl('/home');
  };

  gotoUserProfile = function () {
    this.router.navigateByUrl('/home/userprofile');
  };

  gotoAdminUserSettings = function () {
    this.router.navigateByUrl('/home/admin/user-management');
  };

  gotoAdminRequestSettings = function () {
    this.router.navigateByUrl('home/admin/request-management');
  };

  openContactDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '500px';

    this.dialog.open(ContactDialogComponent, dialogConfig);
  }

  openWhatsapp() {
    window.open('https://web.whatsapp.com/send?phone=918618815857&amp;text=I have some queries about Subhag?', '_blank');
  }

}
