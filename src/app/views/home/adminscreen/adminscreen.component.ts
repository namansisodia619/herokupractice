import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  Router
} from '@angular/router';
import {
  ServiceConfigService
} from '../../../services/service-config/service-config.service';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';

@Component({
  selector: 'app-adminscreen',
  templateUrl: './adminscreen.component.html',
  styleUrls: ['./adminscreen.component.scss']
})
export class AdminscreenComponent implements OnInit {

  constructor(
  ) {}

  ngOnInit() {
  }

}
