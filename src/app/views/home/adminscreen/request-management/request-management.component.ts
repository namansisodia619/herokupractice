import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialog,
  MatDialogConfig
} from '@angular/material';
import {
  ProviderElement
} from '../../../../models/provider.model';
import {
  ServiceElement
} from '../../../../models/service.model';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  BookServiceService
} from '../../../../services/book-service/book-service.service';
import {
  UserDetailDialogComponent
} from '../../../../components/dialogs/user-detail-dialog/user-detail-dialog.component';
import {
  UserDetailService
} from '../../../../services/auth/user-detail.service';
import {
  RequestActionsDialogComponent
} from '../../../../components/dialogs/request-actions-dialog/request-actions-dialog.component';
import {
  AssignProviderDialogComponent
} from '../../../../components/dialogs/assign-provider-dialog/assign-provider-dialog.component';
import {
  ScheduleAppointmentDialogComponent
} from '../../../../components/dialogs/schedule-appointment-dialog/schedule-appointment-dialog.component';
import {
  UserDeleteDialogComponent
} from '../../../../components/dialogs/user-delete-dialog/user-delete-dialog.component';
import {
  BookingCommentsDialogComponent
} from '../../../../components/dialogs/booking-comments-dialog/booking-comments-dialog.component';
import {
  UserQuestionDialogComponent
} from '../../../../components/dialogs/user-question-dialog/user-question-dialog.component';
import {
  Router
} from '@angular/router';


@Component({
  selector: 'app-request-management',
  templateUrl: './request-management.component.html',
  styleUrls: ['./request-management.component.scss']
})
export class RequestManagementComponent implements OnInit {

  loadingProgress = false;
  serviceRequestArray = [];
  serviceRequestArrayConsultation = [];
  serviceRequestArrayIUI = [];
  serviceRequestArraySemenAnalysis = [];
  userDetails = '';

  displayedColumnsSerReq = ['id', 'bookingId', 'createdBy', 'dateCreated', 'bookingDate', 'bookingStatus', 'action'];

  dataSourceSerReqConsult: MatTableDataSource < any > = new MatTableDataSource < any > ();
  dataSourceSerReqIUI: MatTableDataSource < any > = new MatTableDataSource < any > ();
  dataSourceSerReqAnalysis: MatTableDataSource < any > = new MatTableDataSource < any > ();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private localStorage: LocalStorage,
    public dialog: MatDialog,
    private bookServiceService: BookServiceService,
    private userDetailService: UserDetailService,
    private router: Router
  ) {}

  applyFilterSerReq(filterValue: string) {
    this.dataSourceSerReqConsult.filter = filterValue.trim().toLowerCase();
    this.dataSourceSerReqIUI.filter = filterValue.trim().toLowerCase();
    this.dataSourceSerReqAnalysis.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.getUserDetails();
    this.getAllServiceList();
  }

  getUserDetails() {
    this.localStorage.getItem('userDetails').subscribe((userDetails: any) => {
      this.userDetails = userDetails;
      if (!userDetails) {
        this.router.navigateByUrl('/auth');
      }
    });
  }

  getAllServiceList = function () {
    this.getServiceListByServiceId('CONSULTATION');
    this.getServiceListByServiceId('ARTIFICIAL INSEMINATION');
    this.getServiceListByServiceId('SEMEN-ANALYSIS');
  };

  getServiceListByServiceId(serviceID) {
    let tokenValue;
    this.loadingProgress = true;
    this.serviceRequestArrayConsultation = [];
    this.serviceRequestArrayIUI = [];
    this.serviceRequestArraySemenAnalysis = [];
    const serviceIdDetail = {
      serviceId: serviceID,
      userName: ''
    };

    switch (serviceID) {
      case 'CONSULTATION':
        this.localStorage.getItem('userData').subscribe((userData: any) => {
          tokenValue = userData.result.token;
          serviceIdDetail.userName = userData.result.username;
          this.bookServiceService.getServiceBookingListByServiceID(serviceIdDetail, tokenValue).subscribe(
            (data: any) => {
              data.forEach(value => {
                this.serviceRequestArrayConsultation.push({
                  id: value.id,
                  bookingId: value.bookingId,
                  createdBy: value.createdBy,
                  dateCreated: value.dateCreated,
                  bookingDate: value.bookingDate,
                  bookingStatus: value.bookingStatus
                });
              });

              this.loadingProgress = false;
              this.dataSourceSerReqConsult = new MatTableDataSource(this.serviceRequestArrayConsultation);
              this.dataSourceSerReqConsult.paginator = this.paginator;
              this.dataSourceSerReqConsult.sort = this.sort;
            },
            error => {
              this.loadingProgress = false;
              console.error('Error Loggin In!');
            }
          );
        });
        break;
      case 'ARTIFICIAL INSEMINATION':
        this.localStorage.getItem('userData').subscribe((userData: any) => {
          tokenValue = userData.result.token;
          serviceIdDetail.userName = userData.result.username;
          this.bookServiceService.getServiceBookingListByServiceID(serviceIdDetail, tokenValue).subscribe(
            (data: any) => {
              data.forEach(value => {
                this.serviceRequestArrayIUI.push({
                  id: value.id,
                  bookingId: value.bookingId,
                  createdBy: value.createdBy,
                  dateCreated: value.dateCreated,
                  bookingDate: value.bookingDate,
                  bookingStatus: value.bookingStatus
                });
              });

              this.loadingProgress = false;
              this.dataSourceSerReqIUI = new MatTableDataSource(this.serviceRequestArrayIUI);
              this.dataSourceSerReqIUI.paginator = this.paginator;
              this.dataSourceSerReqIUI.sort = this.sort;
            },
            error => {
              this.loadingProgress = false;
              console.error('Error Loggin In!');
            }
          );
        });
        break;
      case 'SEMEN-ANALYSIS':
        this.localStorage.getItem('userData').subscribe((userData: any) => {
          tokenValue = userData.result.token;
          serviceIdDetail.userName = userData.result.username;
          this.bookServiceService.getServiceBookingListByServiceID(serviceIdDetail, tokenValue).subscribe(
            (data: any) => {
              data.forEach(value => {
                this.serviceRequestArraySemenAnalysis.push({
                  id: value.id,
                  bookingId: value.bookingId,
                  createdBy: value.createdBy,
                  dateCreated: value.dateCreated,
                  bookingDate: value.bookingDate,
                  bookingStatus: value.bookingStatus
                });
              });

              this.loadingProgress = false;
              this.dataSourceSerReqAnalysis = new MatTableDataSource(this.serviceRequestArraySemenAnalysis);
              this.dataSourceSerReqAnalysis.paginator = this.paginator;
              this.dataSourceSerReqAnalysis.sort = this.sort;
            },
            error => {
              this.loadingProgress = false;
              console.error('Error Loggin In!');
            }
          );
        });
        break;
    }
  }

  viewUserDetails = function (userName) {
    const dialogConfig = new MatDialogConfig();
    this.localStorage.getItem('userData').subscribe((userData: any) => {

      let userNameObj, tokenValue;
      userNameObj = {
        userName: userName
      };
      tokenValue = userData.result.token;
      this.userDetailService.getUserDetail(userNameObj, tokenValue).subscribe(
        dataValue => {
          dialogConfig.data = {
            userValue: dataValue
          };
          this.dialog.open(UserDetailDialogComponent, dialogConfig);
        },
        error => {
          console.error('Error Loggin In!');
        }
      );
    });

  };


  viewUserComments = function (bookingId) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      bookingId: bookingId
    };
    dialogConfig.width = '800px';
    this.dialog.open(BookingCommentsDialogComponent, dialogConfig);
  };

  deleteBookingRequest = function (bookingId) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      type: 'Booking_Delete',
      bookingID: bookingId
    };

    const dialogRef = this.dialog.open(UserDeleteDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.getAllServiceList();
    });
  };

  openRequestActionDialog = function (requestActionType, bookingID) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      type: requestActionType,
      bookingID: bookingID
    };
    const dialogRef = this.dialog.open(RequestActionsDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.getAllServiceList();
    });
  };

  assignProvider = function (bookingDetail) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      bookingDetail: bookingDetail
    };
    const dialogRef = this.dialog.open(AssignProviderDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.getAllServiceList();
    });
  };

  scheduleAppointment = function (bookingDetail) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      bookingDetail: bookingDetail
    };
    const dialogRef = this.dialog.open(ScheduleAppointmentDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.getAllServiceList();
    });
  };

  openQuestionsDialog = function (bookingId: string) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '500px';
    dialogConfig.data = {
      bookingId: bookingId
    };

    const dialogRef = this.dialog.open(UserQuestionDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  };
}
