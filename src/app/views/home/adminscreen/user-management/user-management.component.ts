import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialogConfig,
  MatDialog
} from '@angular/material';
import {
  ProviderElement
} from '../../../../models/provider.model';
import {
  ServiceElement
} from '../../../../models/service.model';
import {
  Router
} from '@angular/router';
import {
  ServiceConfigService
} from '../../../../services/service-config/service-config.service';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  UserManagementService
} from '../../../../services/user-management/user-management.service';
import {
  UserDetailDialogComponent
} from '../../../../components/dialogs/user-detail-dialog/user-detail-dialog.component';
import {
  BREAKPOINT
} from '@angular/flex-layout';
import {
  UserDeleteDialogComponent
} from '../../../../components/dialogs/user-delete-dialog/user-delete-dialog.component';
import {
  ProviderIdentityDialogComponent
} from '../../../../components/dialogs/provider-identity-dialog/provider-identity-dialog.component';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {

  serviceConfigArray = [];
  providerArray = [];
  managerArray = [];
  usersArray = [];
  loadingProgress = false;
  userDetails = {};
  userAccessRole = '';

  displayedColumnsProvider = ['id', 'firstName', 'userName', 'userPhoneNumber', 'userType', 'active', 'action'];
  displayedColumnsService = ['id', 'title', 'serviceFee', 'roleNurse', 'roleCustomerService', 'roleGynecologist', 'roleAndrologist'];
  displayedColumnsManager = ['id', 'firstName', 'userName', 'userPhoneNumber', 'active', 'action'];
  displayedColumnsUser = ['id', 'firstName', 'userName', 'userPhoneNumber', 'active', 'action'];
  dataSourceProvider: MatTableDataSource < any > = new MatTableDataSource < any > ();
  dataSourceService: MatTableDataSource < any > = new MatTableDataSource < any > ();
  dataSourceManager: MatTableDataSource < any > = new MatTableDataSource < any > ();
  dataSourceUser: MatTableDataSource < any > = new MatTableDataSource < any > ();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private router: Router,
    private serviceConfigService: ServiceConfigService,
    private userManagementService: UserManagementService,
    private localStorage: LocalStorage,
    public dialog: MatDialog
  ) {}

  applyFilterProvider(filterValue: string) {
    this.dataSourceProvider.filter = filterValue.trim().toLowerCase();
  }

  applyFilterService(filterValue: string) {
    this.dataSourceService.filter = filterValue.trim().toLowerCase();
  }

  applyFilterManager(filterValue: string) {
    this.dataSourceManager.filter = filterValue.trim().toLowerCase();
  }

  applyFilterUser(filterValue: string) {
    this.dataSourceUser.filter = filterValue.trim().toLowerCase();
  }


  ngOnInit() {
    this.getUserDetails();
    this.getServiceConfigs();
  }

  gotoManageManager = function () {
    this.router.navigateByUrl('/home/admin/manage-manager');
  };

  gotoManageProvider = function () {
    this.router.navigateByUrl('/home/admin/manage-provider');
  };

  getUserServiceByAccessRoles = function () {
    switch (this.userAccessRole) {
      case 'ROLE_MANAGER':
        this.getUserByRole('USER');
        this.getUserByRole('ROLE_PROVIDER');
        break;
      case 'ADMIN':
        this.getUserByRole('USER');
        this.getUserByRole('ROLE_PROVIDER');
        this.getUserByRole('ROLE_MANAGER');
        break;
    }
  };

  getServiceConfigs() {
    let tokenValue;
    this.loadingProgress = true;
    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      let isRoleNurse, isRoleCustomerService, isRoleGynecologist, isRoleAndrologist = false;

      this.serviceConfigService.getServiceConfigs(tokenValue).subscribe(
        data => {
          data.forEach(value => {
            isRoleNurse = (value.associatedRoles.filter(function (val, key) {
              return val.roleName === 'ROLE_PROVIDER_NURSE';
            }).length) === 1;
            isRoleCustomerService = (value.associatedRoles.filter(function (val, key) {
              return val.roleName === 'ROLE_PROVIDER_CUSTOMER_SERVICE';
            }).length) === 1;
            isRoleGynecologist = (value.associatedRoles.filter(function (val, key) {
              return val.roleName === 'ROLE_PROVIDER_GYNECOLOGIST';
            }).length) === 1;
            isRoleAndrologist = (value.associatedRoles.filter(function (val, key) {
              return val.roleName === 'ROLE_PROVIDER_ANDROLOGIST';
            }).length) === 1;
            this.serviceConfigArray.push({
              id: value.id,
              serviceId: value.serviceId,
              title: value.title,
              serviceFee: value.serviceFee,
              roleNurse: isRoleNurse,
              roleCustomerService: isRoleCustomerService,
              roleGynecologist: isRoleGynecologist,
              roleAndrologist: isRoleAndrologist
            });
          });
          this.loadingProgress = false;
          this.dataSourceService = new MatTableDataSource(this.serviceConfigArray);
          this.dataSourceService.paginator = this.paginator;
          this.dataSourceService.sort = this.sort;
        },
        error => {
          this.loadingProgress = false;
          console.error('Error Loggin In!');
        }
      );
    });

  }

  getUserByRole(roleType) {
    let tokenValue;
    this.loadingProgress = true;
    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      this.userManagementService.getUsersByType(roleType, tokenValue).subscribe((data: any) => {
          switch (roleType) {
            case 'ROLE_PROVIDER':
              this.providerArray = data;
              this.dataSourceProvider = new MatTableDataSource(this.providerArray);
              this.dataSourceProvider.paginator = this.paginator;
              this.dataSourceProvider.sort = this.sort;
              break;
            case 'ROLE_MANAGER':
              this.managerArray = data;
              this.dataSourceManager = new MatTableDataSource(this.managerArray);
              this.dataSourceManager.paginator = this.paginator;
              this.dataSourceManager.sort = this.sort;
              break;
            default:
              this.usersArray = data;
              this.dataSourceUser = new MatTableDataSource(this.usersArray);
              this.dataSourceUser.paginator = this.paginator;
              this.dataSourceUser.sort = this.sort;
          }
        },
        error => {
          this.loadingProgress = false;
          console.error('Error Loggin In!');
        }
      );
    });
  }

  getUserDetails() {
    this.localStorage.getItem('userDetails').subscribe((userDetails: any) => {
      this.userDetails = userDetails;
      if (!userDetails) {
        this.router.navigateByUrl('/auth');
      } else {
        this.userAccessRole = userDetails.userType;
        this.getUserServiceByAccessRoles();
      }
    });
  }

  viewUserDetails = function (detailValue) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      userValue: detailValue,
      width: '500px',
    };

    this.dialog.open(UserDetailDialogComponent, dialogConfig);
  };

  viewProviderIdentityDetails = function (detailValue) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      userValue: detailValue,
      width: '500px',
    };

    this.dialog.open(ProviderIdentityDialogComponent, dialogConfig);
  };

  deleteUser = function (detailValue) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      userID: detailValue,
      width: '500px',
    };

    const dialogRef = this.dialog.open(UserDeleteDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getUserDetails();
      this.getServiceConfigs();
    });
  };
}
