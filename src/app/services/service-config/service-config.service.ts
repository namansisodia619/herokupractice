import {
  Injectable
} from '@angular/core';
import {
  HttpHeaders,
  HttpClient
} from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ServiceConfigService {

  constructor(private http: HttpClient) {}

  getServiceConfigs(token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.get<any[]>(environment.apiUrlBookingManagement + '/api/v1/service/list', httpOptions);
  }

  getServiceQuestions(token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.get<any[]>(environment.apiUrlBookingManagement + '/api/v1/service/getallquestion/service', httpOptions);
  }

  getServiceQuestionsByServiceId(serviceId: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.get<any[]>(environment.apiUrlBookingManagement + '/api/v1/service/getallquestion/service/' + serviceId, httpOptions);
  }

}
