import {
  Injectable
} from '@angular/core';
import {
  HttpHeaders,
  HttpClient
} from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UserManagementService {

  constructor(private http: HttpClient) {}

  getUsersByType(userRoleType: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.get(environment.apiUrlUserManagement + '/api/v1/users/users/' + userRoleType, httpOptions);
  }

  getUserRoles(token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.get<any[]>(environment.apiUrlUserManagement + '/api/v1/users/roles', httpOptions);
  }

  addManagerUser(managerDetail: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.post(environment.apiUrlUserManagement + '/api/v1/users/user/manager', managerDetail, httpOptions);
  }

  addProviderUser(providerDetail: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.post(environment.apiUrlUserManagement + '/api/v1/users/user/provider', providerDetail, httpOptions);
  }

  getProviderIdentity(providerIdDetail: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.post(environment.apiUrlUserManagement + '/api/v1/users/get-provider-identity', providerIdDetail, httpOptions);
  }

  createProviderIdentity(providerIdDetail: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.post(environment.apiUrlUserManagement + '/api/v1/users/add-provider-identity', providerIdDetail, httpOptions);
  }

  updateProviderIdentity(providerIdDetail: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.put(environment.apiUrlUserManagement + '/api/v1/users/update-provider-identity', providerIdDetail, httpOptions);
  }

  deleteUser(userID: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.delete(environment.apiUrlUserManagement + '/api/v1/users/user/' + userID, httpOptions);
  }

}
