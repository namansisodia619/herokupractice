import {
  Injectable
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  environment
} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookServiceService {

  constructor(private http: HttpClient) {}

  bookService(bookServiceDetail, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        // 'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.post(environment.apiUrlBookingManagement + '/api/v1/booking/add', bookServiceDetail, httpOptions);
  }

  getServiceBookingList(token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.get(environment.apiUrlBookingManagement + '/api/v1/booking/list', httpOptions);
  }

  getServiceBookingListByServiceID(serviceIdDetail, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.post(environment.apiUrlBookingManagement + '/api/v1/booking/getbookingbyservice', serviceIdDetail, httpOptions);
  }

  getServiceBookingListByUserName(userNameDetail, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.post(environment.apiUrlBookingManagement + '/api/v1/booking/getbookingbyusername', userNameDetail, httpOptions);
  }

  getAnswersByBookingId(bookingId, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.get(environment.apiUrlBookingManagement + '/api/v1/booking/getanswerbybookingid/booking/' + bookingId, httpOptions);
  }

  getBookingCommentByBookingId(bookingId, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.get(environment.apiUrlBookingManagement + '/api/v1/booking/getbookingcomment/booking/' + bookingId, httpOptions);
  }

  addBookingCommentByBookingId(bookingCommentDetails, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.post(environment.apiUrlBookingManagement + '/api/v1/booking/addusercomment/booking', bookingCommentDetails, httpOptions);
  }

  assignProviders(providerDetails, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.put(environment.apiUrlBookingManagement + '/api/v1/booking/assignproviders', providerDetails, httpOptions);
  }

  setAppointmentDate(appointmentDetails, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.put(environment.apiUrlBookingManagement + '/api/v1/booking/bookingscheduled', appointmentDetails, httpOptions);
  }

  startConsultation(consultationDetails, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.put(environment.apiUrlBookingManagement + '/api/v1/booking/bookingongoing', consultationDetails, httpOptions);
  }

  completeConsultation(consultationDetails, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.put(environment.apiUrlBookingManagement + '/api/v1/booking/bookingcomplete', consultationDetails, httpOptions);
  }

  deleteBookingRequest(bookingId, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.delete(environment.apiUrlBookingManagement + '/api/v1/booking/deletebooking/' + bookingId, httpOptions);
  }

  addBookingAnswers(bookingAnswerDetails, token) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.post(environment.apiUrlBookingManagement + '/api/v1/booking/addanswer/booking', bookingAnswerDetails, httpOptions);
  }
}
