import {
  Injectable
} from '@angular/core';
import {
  HttpHeaders,
  HttpClient
} from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UserDetailService {

  constructor(private http: HttpClient) {}

  getUserDetail(userName: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.post(environment.apiUrlUserManagement + '/api/v1/users/user', userName, httpOptions);
  }

  getUserPartnerDetail(userName: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.post(environment.apiUrlUserManagement + '/api/v1/users/getspouse', userName, httpOptions);
  }

  updateUserDetail(userDetail: any, token: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.put(environment.apiUrlUserManagement + '/api/v1/users/updateuser', userDetail, httpOptions);
  }

  addSpouseDetail(userSpouseDetail: any, token: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.post(
      environment.apiUrlUserManagement + '/api/v1/users/addspouse',
      userSpouseDetail,
      httpOptions);
  }

  updateSpouseDetail(userSpouseDetail: any, token: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    return this.http.put(
      environment.apiUrlUserManagement + '/api/v1/users/updatespouse',
      userSpouseDetail,
      httpOptions);
  }

  sendOTPForMobileVerfication(mobileNumber: any, token: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };

    const url = environment.apiUrlUserManagement + '/api/v1/users/mobilenumber/' + mobileNumber + '/sendOtp';
    return this.http.get(
      url,
      httpOptions);
  }

  verifyOTPForMobile(verificationValue: any, token: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    // tslint:disable-next-line:max-line-length
    const url = environment.apiUrlUserManagement + '/api/v1/users/mobilenumber/verifyOtp';
    return this.http.post(
      url, verificationValue, httpOptions);
  }

}
