import {
  Injectable
} from '@angular/core';
import {
  HttpHeaders,
  HttpClient
} from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class SignInService {

  constructor(private http: HttpClient) {}

  loginUser(userDetail) {

    const httpOptions = {
      headers: new HttpHeaders({
        // 'Accept': 'application/json',
        'Content-Type': 'application/json'
      })
    };

    return this.http.post(environment.apiUrlUserManagement + '/login/generate-token', userDetail, httpOptions);
  }
}
