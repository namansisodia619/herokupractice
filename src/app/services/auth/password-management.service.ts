import {
  Injectable
} from '@angular/core';
import {
  HttpHeaders,
  HttpClient
} from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class PasswordManagementService {

  constructor(private http: HttpClient) {}

  changePassword(userPasswordDetail) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.post(environment.apiUrlUserManagement + '/login/changepassword', userPasswordDetail, httpOptions);
  }

  sendResetPasswordLink(userPasswordDetail) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    // tslint:disable-next-line:max-line-length
    return this.http.post(environment.apiUrlUserManagement + '/login/forgotpwd', userPasswordDetail, httpOptions);
  }
}
