import { TestBed } from '@angular/core/testing';

import { PasswordManagementService } from './password-management.service';

describe('PasswordManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PasswordManagementService = TestBed.get(PasswordManagementService);
    expect(service).toBeTruthy();
  });
});
