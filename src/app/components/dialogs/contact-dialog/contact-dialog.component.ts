import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';

@Component({
  selector: 'app-contact-dialog',
  templateUrl: './contact-dialog.component.html',
  styleUrls: ['./contact-dialog.component.scss']
})
export class ContactDialogComponent implements OnInit {

  constructor(
    public thisDialogRef: MatDialogRef < ContactDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit() {

  }

}
