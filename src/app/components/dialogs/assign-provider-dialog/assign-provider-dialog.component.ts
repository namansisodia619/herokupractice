import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  UserManagementService
} from '../../../services/user-management/user-management.service';
import {
  BookServiceService
} from '../../../services/book-service/book-service.service';

@Component({
  selector: 'app-assign-provider-dialog',
  templateUrl: './assign-provider-dialog.component.html',
  styleUrls: ['./assign-provider-dialog.component.scss']
})
export class AssignProviderDialogComponent implements OnInit {

  providerList = [];
  providerCSList = [];
  providerGYList = [];
  providerDetails = {
    bookingId: '',
    providers: [],
    userName: ''
  };
  gynaecologist = '';
  customersupport = '';
  loadingProgress = false;
  isProvidersNotAssigned = false;
  isProvidersAssigned = false;

  constructor(
    public thisDialogRef: MatDialogRef < AssignProviderDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private localStorage: LocalStorage,
    private userManagementService: UserManagementService,
    private bookServiceService: BookServiceService
  ) {}

  ngOnInit() {
    this.getProviderDetails();
  }

  getProviderDetails() {
    let tokenValue;
    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      this.userManagementService.getUsersByType('ROLE_PROVIDER', tokenValue).subscribe((data: any) => {
          this.providerList = data;
          this.providerCSList = this.providerList.filter(value => value.userType === 'ROLE_PROVIDER_CUSTOMER_SERVICE');
          this.providerGYList = this.providerList.filter(value => value.userType === 'ROLE_PROVIDER_GYNECOLOGIST');
        },
        error => {
          console.error('Error Loggin In!');
        }
      );
    });
  }

  assignSelectedProviders() {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      console.log(this.data.bookingDetail);

      this.providerDetails.providers.push(this.customersupport);
      this.providerDetails.providers.push(this.gynaecologist);
      this.providerDetails.bookingId = this.data.bookingDetail.bookingId;
      this.providerDetails.userName = this.data.bookingDetail.createdBy;

      this.bookServiceService.assignProviders(this.providerDetails, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.isProvidersAssigned = true;
          console.log(data);
        },
        error => {
          this.loadingProgress = false;
          this.isProvidersNotAssigned = true;
          console.error('Error Loggin In!');
        }
      );
    });
  }

}