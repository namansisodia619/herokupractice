import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';
import {
  BookServiceService
} from '../../../services/book-service/book-service.service';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';

@Component({
  selector: 'app-booking-comments-dialog',
  templateUrl: './booking-comments-dialog.component.html',
  styleUrls: ['./booking-comments-dialog.component.scss']
})
export class BookingCommentsDialogComponent implements OnInit {

  bookingCommentDetail = {
    bookingId: this.data.bookingId,
    bookingComment: '',
    userName: ''
  };

  bookingCommentsArray = [];

  constructor(
    public thisDialogRef: MatDialogRef < BookingCommentsDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private bookServiceService: BookServiceService,
    private localStorage: LocalStorage
  ) {}

  ngOnInit() {
    this.getBookingComments();
  }

  addBookingComments = function () {
    let tokenValue;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      this.bookingCommentDetail.userName = userData.result.username;

      this.bookServiceService.addBookingCommentByBookingId(this.bookingCommentDetail, tokenValue).subscribe(
        dataValue => {
          console.log(dataValue);
          this.bookingCommentDetail = {
            bookingId: this.data.bookingId,
            bookingComment: '',
            userName: ''
          };
          this.getBookingComments();
        },
        error => {
          console.error('Error Loggin In!');
        }
      );
    });
  };

  getBookingComments = function () {
    let tokenValue;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.bookServiceService.getBookingCommentByBookingId(this.data.bookingId, tokenValue).subscribe(
        dataValue => {
          this.bookingCommentsArray = dataValue.reverse();
        },
        error => {
          console.error('Error Loggin In!');
        }
      );
    });

  };

}
