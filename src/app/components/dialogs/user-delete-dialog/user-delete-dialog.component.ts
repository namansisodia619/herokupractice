import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  UserManagementService
} from '../../../services/user-management/user-management.service';
import { BookServiceService } from '../../../services/book-service/book-service.service';

@Component({
  selector: 'app-user-delete-dialog',
  templateUrl: './user-delete-dialog.component.html',
  styleUrls: ['./user-delete-dialog.component.scss']
})
export class UserDeleteDialogComponent implements OnInit {

  loadingProgress = false;
  deleteSuccess = false;
  deleteFailure = false;
  isBookingRequest = false;
  isUserRequest = false;

  constructor(
    public thisDialogRef: MatDialogRef < UserDeleteDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private localStorage: LocalStorage,
    private userManagementService: UserManagementService,
    private bookServiceService: BookServiceService
  ) {}

  ngOnInit() {
    if (this.data.type === 'Booking_Delete') {
      this.isBookingRequest = true;
      this.deleteRequest();
    } else {
      this.isUserRequest = true;
      this.deleteUser();
    }
  }

  deleteUser() {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.userManagementService.deleteUser(this.data.userID, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.deleteSuccess = true;
          console.log(data);
        },
        error => {
          this.loadingProgress = false;
          this.deleteFailure = true;
          console.error('Error Loggin In!');
        }
      );
    });
  }

  deleteRequest() {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.bookServiceService.deleteBookingRequest(this.data.bookingID, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.deleteSuccess = true;
          console.log(data);
        },
        error => {
          this.loadingProgress = false;
          this.deleteFailure = true;
          console.error('Error Loggin In!');
        }
      );
    });
  }

}
