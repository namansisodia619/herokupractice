import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  UserManagementService
} from '../../../services/user-management/user-management.service';

@Component({
  selector: 'app-provider-identity-dialog',
  templateUrl: './provider-identity-dialog.component.html',
  styleUrls: ['./provider-identity-dialog.component.scss']
})
export class ProviderIdentityDialogComponent implements OnInit {

  providerIdentityDetail = {
    id: null,
    identityNumber: '',
    identityType: '',
    registeredEmployeeId: '',
    status: 'Verified'
  };

  isProviderIdAvailable = true;
  loadingProgress = false;
  isSuccess = false;
  isFailure = false;
  isUpdateSubmit = false;

  constructor(
    public thisDialogRef: MatDialogRef < ProviderIdentityDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userManagementService: UserManagementService,
    private localStorage: LocalStorage
  ) {}

  ngOnInit() {
    this.getProviderIdentityDetails();
  }

  getProviderIdentityDetails() {
    let tokenValue;
    this.loadingProgress = true;
    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      const providerIdDetail = {
        userName: this.data.userValue.userName
      };

      this.userManagementService.getProviderIdentity(providerIdDetail, tokenValue).subscribe(
        (data: any) => {
          this.loadingProgress = false;
          this.providerIdentityDetail = data;
          this.isProviderIdAvailable = true;
        },
        error => {
          this.isProviderIdAvailable = false;
          this.loadingProgress = false;
          console.error('Error Loggin In!');
        }
      );
    });

  }

  addProviderIdentity = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      this.providerIdentityDetail.userName = this.data.userValue.userName;
      delete this.providerIdentityDetail.id;
      this.userManagementService.createProviderIdentity(this.providerIdentityDetail, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.isSuccess = true;
        },
        error => {
          this.loadingProgress = false;
          this.isFailure = true;
          console.error('Error Loggin In!');
        }
      );
    });
  };

  updateProviderIdentity = function () {
    let tokenValue;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      this.userManagementService.updateProviderIdentity(this.providerIdentityDetail, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.isSuccess = true;
        },
        error => {
          this.loadingProgress = false;
          this.isFailure = true;
          console.error('Error Loggin In!');
        }
      );
    });
  };

  editProviderIdentity = function () {
    this.isProviderIdAvailable = false;
    this.isUpdateSubmit = true;
  };

}
