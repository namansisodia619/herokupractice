import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialog,
  MatDialogConfig
} from '@angular/material';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  BookServiceService
} from '../../../services/book-service/book-service.service';
import {
  UserQuestionDialogComponent
} from '../user-question-dialog/user-question-dialog.component';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-book-service-dialog',
  templateUrl: './book-service-dialog.component.html',
  styleUrls: ['./book-service-dialog.component.scss']
})
export class BookServiceDialogComponent implements OnInit {

  loadingProgress = false;
  bookingSuccess = false;
  bookingFailure = false;
  bookingNotSupported = false;
  successBookingId = '';
  isPreviousBookingAvailable = false;

  bookServiceDetail = {
    'bookingId': '',
    'bookingAmount': null,
    'bookingDate': new Date(),
    'providers': [
      ''
    ],
    'serviceId': '',
    'userName': ''
  };

  constructor(
    public thisDialogRef: MatDialogRef < BookServiceDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private localStorage: LocalStorage,
    private bookServiceService: BookServiceService,
    public dialog: MatDialog,
    private router: Router,
  ) {}

  ngOnInit() {
    if (this.data.serviceDetail.title === 'CONSULTATION') {
      this.validateBookingRequest();
    } else {
      this.bookingNotSupported = true;
    }
  }

  validateBookingRequest = function () {

    let tokenValue;
    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      const userNameDetail = {
        userName: userData.result.username
      };
      this.bookServiceService.getServiceBookingListByUserName(userNameDetail, tokenValue).subscribe(
        data => {
          if (data.length) {
            this.isPreviousBookingAvailable = true;
          } else {
            this.bookService();
          }
        },
        error => {
          console.error('Error Loggin In!');
        }
      );
    });
  };
  bookService() {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.bookServiceDetail = {
        'bookingId': '',
        'bookingAmount': this.data.serviceDetail.serviceFee,
        'bookingDate': new Date(),
        'providers': [
          ''
        ],
        'serviceId': this.data.serviceDetail.serviceId,
        'userName': userData.result.username
      };

      this.bookServiceService.bookService(this.bookServiceDetail, tokenValue).subscribe(
        (data: any) => {
          this.loadingProgress = false;
          this.bookingSuccess = true;
          this.successBookingId = data.bookingId;
        },
        error => {
          this.loadingProgress = false;
          this.bookingFailure = true;
          console.error('Error Loggin In!');
        }
      );
    });
  }

  openQuestionsDialog = function () {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '500px';
    dialogConfig.data = {
      bookingId: this.successBookingId
    };

    const dialogRef = this.dialog.open(UserQuestionDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      this.router.navigateByUrl('/home');
    });
  };

}