import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';

@Component({
  selector: 'app-generic-status-dialog',
  templateUrl: './generic-status-dialog.component.html',
  styleUrls: ['./generic-status-dialog.component.scss']
})
export class GenericStatusDialogComponent implements OnInit {

  constructor(
    public thisDialogRef: MatDialogRef < GenericStatusDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private localStorage: LocalStorage
  ) {}

  ngOnInit() {
  }

}
