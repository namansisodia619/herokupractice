import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  ServiceConfigService
} from '../../../services/service-config/service-config.service';
import {
  BookServiceService
} from '../../../services/book-service/book-service.service';

@Component({
  selector: 'app-user-question-dialog',
  templateUrl: './user-question-dialog.component.html',
  styleUrls: ['./user-question-dialog.component.scss']
})
export class UserQuestionDialogComponent implements OnInit {

  loadingProgress = false;
  bookingQuestionDetail = [];
  bookingAnswerDetails = {
    answer: '',
    bookingId: '',
    userName: ''
  };
  answerSubmitSuccess = false;
  answerSubmitFailure = false;
  isDataEntryDisabled = false;

  constructor(
    public thisDialogRef: MatDialogRef < UserQuestionDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private localStorage: LocalStorage,
    private serviceConfigService: ServiceConfigService,
    private bookServiceService: BookServiceService
  ) {}

  ngOnInit() {
    this.getUserServiceAnswer();
  }

  getAllServiceQuestions = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.serviceConfigService.getServiceQuestions(tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          const userBookingQuestions = [];
          data.forEach(function (element) {
            userBookingQuestions.push({
              question: element.question,
              answer: ''
            });
          });
          this.bookingQuestionDetail = userBookingQuestions;
        },
        error => {
          this.loadingProgress = false;
          console.error('Error Loggin In!');
        }
      );
    });
  };


  getUserServiceAnswer = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.bookServiceService.getAnswersByBookingId(this.data.bookingId, tokenValue).subscribe(
        data => {
          if (data.length > 0) {
            this.isDataEntryDisabled = true;
            const userBookingQuestions = [];

            let userAnswer = data.pop().answer;
            userAnswer = JSON.parse(userAnswer);

            userAnswer.forEach(function (element) {
              userBookingQuestions.push({
                question: element.question,
                answer: element.answer
              });
            });
            this.bookingQuestionDetail = userBookingQuestions;
            this.loadingProgress = false;
          } else {
            this.isDataEntryDisabled = false;
            this.getAllServiceQuestions();
          }
        },
        error => {
          this.loadingProgress = false;
          console.error('Error Loggin In!');
        }
      );
    });
  };

  submitAllAnswers = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      const answerDetailsString = JSON.stringify(this.bookingQuestionDetail);
      this.bookingAnswerDetails = {
        answer: answerDetailsString,
        bookingId: this.data.bookingId,
        userName: userData.result.username
      };

      this.bookServiceService.addBookingAnswers(this.bookingAnswerDetails, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.answerSubmitSuccess = true;

        },
        error => {
          this.loadingProgress = false;
          this.answerSubmitFailure = true;
          console.error('Error Loggin In!');
        }
      );
    });
  };

}
