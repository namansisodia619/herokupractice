import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import { UserManagementService } from '../../../services/user-management/user-management.service';
import { BookServiceService } from '../../../services/book-service/book-service.service';

@Component({
  selector: 'app-schedule-appointment-dialog',
  templateUrl: './schedule-appointment-dialog.component.html',
  styleUrls: ['./schedule-appointment-dialog.component.scss']
})
export class ScheduleAppointmentDialogComponent implements OnInit {

  maxDate = new Date(Date.now());
  startDate = this.maxDate;
  scheduledDate: any = '';
  scheduledTime = 0;
  bookingDetails = {
    bookingId: '',
    bookingDate: ''
  };
  loadingProgress = false;
  isAppointmentScheduled = false;
  isAppointmentNotScheduled = false;

  constructor(
    public thisDialogRef: MatDialogRef < ScheduleAppointmentDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private localStorage: LocalStorage,
    private userManagementService: UserManagementService,
    private bookServiceService: BookServiceService
  ) { }

  ngOnInit() {
  }

  setAppointmentDate() {
    let tokenValue;
    this.loadingProgress = true;

    const updatedDateTime = new Date(this.scheduledDate.setHours(this.scheduledTime));

    this.localStorage.getItem('userData').subscribe((userData: any) => {
        tokenValue = userData.result.token;
        this.bookingDetails.bookingId = this.data.bookingDetail.bookingId;
        this.bookingDetails.bookingDate = updatedDateTime.toISOString();

        this.bookServiceService.setAppointmentDate(this.bookingDetails, tokenValue).subscribe(
          data => {
            this.loadingProgress = false;
            this.isAppointmentScheduled = true;
            console.log(data);
          },
          error => {
            this.loadingProgress = false;
            this.isAppointmentNotScheduled = true;
            console.error('Error Loggin In!');
          }
        );
      });
    }

}
