import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  UserDetailService
} from '../../../services/auth/user-detail.service';

@Component({
  selector: 'app-mobile-verification-dialog',
  templateUrl: './mobile-verification-dialog.component.html',
  styleUrls: ['./mobile-verification-dialog.component.scss']
})
export class MobileVerificationDialogComponent implements OnInit {

  loadingProgress = false;
  verificationSuccess = false;
  verificationFailure = false;
  sendOTPSuccess = false;
  sendOTPFailure = false;
  mobileVerification = {
    mobileNumber: '',
    otp: '',
    userName: ''
  };

  constructor(
    public thisDialogRef: MatDialogRef < MobileVerificationDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userDetailService: UserDetailService,
    private localStorage: LocalStorage
  ) {}

  ngOnInit() {
    this.setDialogStatus();
  }

  setDialogStatus = function () {
    switch (this.data.dialogStatus) {
      case 'sendOTPSuccess':
        this.sendOTPSuccess = true;
        break;
      case 'sendOTPFailure':
        this.sendOTPFailure = true;
        break;
    }
  };

  resetStatus = function () {
    this.verificationSuccess = false;
    this.verificationFailure = false;
    this.sendOTPSuccess = false;
    this.sendOTPFailure = false;
  };

  verifyMobileOTP = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;
      this.localStorage.getItem('userDetails').subscribe((userDetails: any) => {

        this.mobileVerification.mobileNumber = '+91' + userDetails.userPhoneNumber;
        this.mobileVerification.userName = userDetails.userName;

        this.userDetailService.verifyOTPForMobile(this.mobileVerification, tokenValue).subscribe(
          data => {
            this.resetStatus();
            this.loadingProgress = false;
            this.verificationSuccess = true;
          },
          error => {
            this.resetStatus();
            this.loadingProgress = false;
            this.verificationFailure = true;
          });
      });
    });
  };

}
