import {
  Component,
  OnInit,
  Inject
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef
} from '@angular/material';
import {
  LocalStorage
} from '@ngx-pwa/local-storage';
import {
  UserManagementService
} from '../../../services/user-management/user-management.service';
import {
  BookServiceService
} from '../../../services/book-service/book-service.service';


@Component({
  selector: 'app-request-actions-dialog',
  templateUrl: './request-actions-dialog.component.html',
  styleUrls: ['./request-actions-dialog.component.scss']
})
export class RequestActionsDialogComponent implements OnInit {

  bookingStartDetails = {
    bookingId: ''
  };
  bookingCompleteDetails = {
    bookingId: ''
  };

  loadingProgress = false;
  isConsultStarted = false;
  isConsultNotStarted = false;
  isConsultCompleted = false;
  isConsultNotCompleted = false;

  constructor(
    public thisDialogRef: MatDialogRef < RequestActionsDialogComponent > ,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private localStorage: LocalStorage,
    private userManagementService: UserManagementService,
    private bookServiceService: BookServiceService
  ) {}

  ngOnInit() {
    switch (this.data.type) {
      case 'CONSULT_START':
        this.startConsultation();
        break;
      case 'CONSULT_COMPLETE':
        this.completeConsultation();
        break;
    }
  }

  startConsultation = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.bookingStartDetails.bookingId = this.data.bookingID;

      this.bookServiceService.startConsultation(this.bookingStartDetails, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.isConsultStarted = true;
          console.log(data);
        },
        error => {
          this.loadingProgress = false;
          this.isConsultNotStarted = true;
          console.error('Error Loggin In!');
        }
      );
    });
  };

  completeConsultation = function () {
    let tokenValue;
    this.loadingProgress = true;

    this.localStorage.getItem('userData').subscribe((userData: any) => {
      tokenValue = userData.result.token;

      this.bookingCompleteDetails.bookingId = this.data.bookingID;

      this.bookServiceService.completeConsultation(this.bookingCompleteDetails, tokenValue).subscribe(
        data => {
          this.loadingProgress = false;
          this.isConsultCompleted = true;
          console.log(data);
        },
        error => {
          this.loadingProgress = false;
          this.isConsultNotCompleted = true;
          console.error('Error Loggin In!');
        }
      );
    });
  };

}