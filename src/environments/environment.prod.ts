export const environment = {
  production: true,
  apiUrlUserManagement: 'https://services-subhag.ml:9443',
  apiUrlBookingManagement: 'https://services-subhag.ml:9444'
};
